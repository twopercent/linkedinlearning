#!/usr/bin/python3


def cleanstring(string):
  output = ""
  for letter in string:
    if letter.isalpha():
      output += letter.lower()
  return output

def is_palindrome(testword):
    teststr = cleanstring(testword)
    while len(teststr) > 1:
      print("greater than 1")
      if teststr[0] == teststr[-1]:
        teststr = teststr.removeprefix(teststr[0])
        teststr = teststr.removesuffix(teststr[-1])
        print(teststr)
      else:
        return False
    print(testword, 'is a palindrome!')
    return True


is_palindrome("test")
is_palindrome("racei car")
is_palindrome("race car")
is_palindrome("donkey")
is_palindrome("$ferlslRef33")

print(len("racecar"))

print(cleanstring("d93;;!DGWirty"))
