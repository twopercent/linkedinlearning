#!/usr/bin/python3

import os
from os import path
import datetime
from datetime import date, time, timedelta
import time

def main():
  print(os.name)
  print("Item exists:", path.exists("textfile.txt"))
  print("Item is  a file:", path.isfile("textfile.txt"))
  print("Item is a directory:", path.isdir("textfile.txt"))

  print("Items path:", path.realpath("textfile.txt"))
  print("Items path and name:", path.split(path.realpath("textfile.txt")))

  t = time.ctime(path.getmtime("textfile.txt"))
  print(t)
  print(datetime.datetime.fromtimestamp(path.getmtime("textfile.txt")))

  td = datetime.datetime.now() - datetime.datetime.fromtimestamp(path.getmtime("textfile.txt"))
  print("It has been", td, "since the file has been modified")
  print("Or,", td.total_seconds(), "seconds")

if __name__ == ("__main__"):
  main()
