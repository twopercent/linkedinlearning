#!/usr/bin/python3

import os
from os import path
import shutil


def main():
  if path.exists("textfile.txt.bak"):
    src = path.realpath("textfile.txt")
    dst = src + ".bak"
    shutil.copy(src, dst)

    root_dir, tail = path.split(src)
    shutil.make_archive("archive", "zip", root_dir)


if __name__ == ("__main__"):
  main()
