#!/usr/bin/python3

def main():
  try:
    answer = input("what should I divide 10 by? ")
    num = int(answer)
    print(10 / num)
  except ZeroDivisionError as e:
    print("You can't divide by 0")
  except ValueError as e:
    print("You didn't give me a valid number!")
    print(e)
  finally:
    print("This code always run")

if __name__ == ("__main__"):
  main()
