#!/bin/bash/python3

def factorial(num):
    result = 1
    try:
        while num > 0:
            result = result * num
            num = num - 1
        return result
    except TypeError:
        print("Value is not a number")



print(factorial(3))
print(factorial(4))
print(factorial(5))
print(factorial(-2))
print(factorial(2.4))
print(factorial('words'))
