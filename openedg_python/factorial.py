#!/bin/bash/python3

def factorial(num):
    result = 1
    if type(num) != int:
        return None
    elif num < 0:
        return None
    else:
        while num > 0:
            result = result * num
            num = num - 1
        return result

print(factorial(3))
print(factorial(4))
print(factorial(5))
print(factorial(-2))
print(factorial('words'))
